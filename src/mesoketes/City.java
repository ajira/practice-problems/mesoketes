package mesoketes;

import java.util.HashMap;

public class City {
    private final HashMap<String, Wall> walls;

    public City(String[] wallNames) {
        walls = new HashMap<>();
        for (String wallName : wallNames) {
            walls.put(wallName, new Wall());
        }
    }

    public boolean defendWall(String wallName, Integer attackStrength) {
        return walls.get(wallName).absorbAttack(attackStrength);
    }

    public void raiseWallHeights() {
        walls.forEach((k, w) -> w.raiseWallHeight());
    }

}
