package mesoketes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    private static String DAY_SEPARATOR = ";";
    private static String DAY_TRIBE_SEPARATOR = "\\$";
    private static String TRIBE_SEPARATOR = ":";
    private static String ATTACK_COMPONENT_SEPARATOR = "-";

    public static void main(String[] args) throws IOException {
        City mesoketes = new City(new String[]{"N", "S", "E", "W"});
        String input = new BufferedReader(new InputStreamReader(System.in)).readLine();

        String[] battleDays = input.split(DAY_SEPARATOR);
        Stream<Day> daysOfAttack = Arrays.stream(battleDays).map(day -> parseDay(day));

        ArrayList<Integer> successfulAttacks = new ArrayList<>();
        daysOfAttack.forEach(currentDay -> {
            int successfulAttacksCount = currentDay.computeSuccessfulAttacks(mesoketes);
            mesoketes.raiseWallHeights();
            successfulAttacks.add(successfulAttacksCount);
        });

        successfulAttacks.stream().reduce((x, y) -> x + y)
                .ifPresent(System.out::println);
    }

    private static Day parseDay(String day) {
        String[] dayTribeAttackComponents = day.split(DAY_TRIBE_SEPARATOR);
        String tribalAttacksString = dayTribeAttackComponents[1];
        String[] attacks = tribalAttacksString.split(TRIBE_SEPARATOR);
        List<TribalAttack> tribalAttacks = Arrays.stream(attacks).map(Main::parseTribalAttack).collect(Collectors.toList());
        return new Day(tribalAttacks);
    }

    private static TribalAttack parseTribalAttack(String attackString) {
        String[] attackComponents = attackString.split(ATTACK_COMPONENT_SEPARATOR);
        String wallName = attackComponents[1].trim();
        Integer attackStrength = new Integer(attackComponents[3].trim());
        return new TribalAttack(wallName, attackStrength);
    }
}
