package mesoketes;

/**
 * Created by ajira on 18/06/19.
 */
public class TribalAttack {
    private String wallName;
    private int attackStrength;

    public TribalAttack(String wall, int strength) {
        wallName = wall;
        attackStrength = strength;
    }

    public String getWallName () {
        return wallName;
    }

    public int getAttackStrength (){
        return attackStrength;
    }
}
