package mesoketes;

public class Wall {
    int height;
    int maxAttackReceivedBeforeHeightChange;

    public Wall() {
        height = 0;
        maxAttackReceivedBeforeHeightChange = 0;
    }

    public Boolean absorbAttack(int strength) {
        if (maxAttackReceivedBeforeHeightChange < strength) {
            maxAttackReceivedBeforeHeightChange = strength;
        }
        return height < strength;
    }

    public void raiseWallHeight() {
        if (maxAttackReceivedBeforeHeightChange > height) {
            height = maxAttackReceivedBeforeHeightChange;
        }
        maxAttackReceivedBeforeHeightChange = 0;
    }
}
